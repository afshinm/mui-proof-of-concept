import React from "react";
import {
  Box, 
  AppBar,
  Toolbar,
  IconButton,
} from "@mui/material";
import OomaIcon from "./OomaIcon1";

interface AppProps {
  children: React.ReactNode;
}

// Update the Button's color prop options
declare module '@mui/material/AppBar' {
  interface AppBarPropsColorOverrides {
    graphite1: true;
  }
}

function HeaderWithLogo() {
  return (
    <AppBar
      position="sticky"
      color="graphite1"
      sx={{ maxWidth: "1230px", margin: "0 auto",
    // background: 'graphite1'
   }}
    >
      <Toolbar>
        <IconButton aria-label="menu">
          <OomaIcon />

        </IconButton>
      </Toolbar>
    </AppBar>
  );
}
function Blankpage({ children }: AppProps) {
  return (
    <Box width="100%" minHeight="100vh" bgcolor="graphite1">
      <HeaderWithLogo />
      {children}
    </Box>
  );
}

export default Blankpage;
