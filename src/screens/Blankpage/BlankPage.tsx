import React from "react";
import {
  Box,
  Grid,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button,
} from "@mui/material";
import OomaIcon from "./OomaIcon1";

interface AppProps {
  children: React.ReactNode;
}

function HeaderWithLogo() {
  return (
    <Box
      component="header"
      sx={{
        height: "64px",
        width: "100%",
        maxWidth: "1230px", // 1198 + ( 16px x 2 )
        display: "flex",
        alignItems: "center",
        margin: "0 auto",
        pl: 4,
      }}
    >
      <OomaIcon />
    </Box>
  );
}
function Blankpage({ children }: AppProps) {
  return (
    <Box width="100%" minHeight="100vh" bgcolor="graphite1">
      <HeaderWithLogo />
      {children}
    </Box>
  );
}

export default Blankpage;