import React from "react";
import {
  FormControl,
  InputLabel,
  Input,
  FormHelperText,
  Stack,
  // TextField,
} from "@mui/material";
import WarningIcon from "@mui/icons-material/Warning";

/*
  - NOT USED!!!!!            -- First attempt at the custom input...
  - implementation using functions is most flexible but also obligates dev using system to know about
    new components that aren't in Mui docs... so not using this implementation when I can do it all with
    css tweaks at the theme level.

  Input Props: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#attributes
*/

type OomaTextInputProps = {
  label: string;
  inputId: string;
  helperText?: string;
  error?: boolean;
  InputProps?: object;  // TODO: typing?
  InputLabelProps?: object;
  // All other props
  // [x: string]: any;
};

export default function OomaTextInput({
  label,
  helperText = "",
  inputId,
  error = false,
  InputProps,
  InputLabelProps,
}: OomaTextInputProps) {
  return (
    <FormControl variant="standard">
      <InputLabel
        htmlFor={inputId}
        disableAnimation={true}
        shrink={true}
        color="secondary"
        sx={{
          // mt: "-9px",
          // ml: "-12px",
          fontSize: "14px",
          lineHeight: "20px",
          letterSpacing: "-.25 px",
        }}
        // inputLabelProps={InputLabelProps}
      >
        {label}
      </InputLabel>
      <Input
        id={inputId}
        aria-describedby={`${inputId}-helper-text`}
        disableUnderline
        error={error}
        placeholder="Text"
        sx={{
          border: "2px solid #5D6FD4",
          borderRadius: "8px",
          height: "42px",
          lineHeight: "24px",
          pl: "12px",
        }}
        inputProps={InputProps}
      />
      <Stack direction="row" mt="2px">
        {error && <WarningIcon color="error" sx={{ mr: 1 }} />}
        <FormHelperText
          id={`${inputId}-helper-text`}
          error={error}
          sx={{
            ml: 0,
            fontSize: "12px",
            lineHeight: "16px",
            color: "text.secondary",
          }}
        >
          {helperText}
        </FormHelperText>
      </Stack>
    </FormControl>
  );
}
