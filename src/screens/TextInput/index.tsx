import React from "react";
import {
  // Box, Container, Divider,
  Stack,
  TextField,
} from "@mui/material";

export default function TextInputScreen() {
  return (
    <main>
      <Stack direction="row" m="50px" padding="10px" spacing={12}>
        <Stack spacing={3} width="360px">
          <TextField id="custom00" />
          <TextField
            id="custom01"
            label="Label here"
            placeholder="placeholder Text"
          />       
          <TextField
            id="custom02"
            label="Label lights up on focus!"
            placeholder="placeholder Text"
            helperText="Message helperText"
          />
          <TextField
            id="custom03"
            label="Error with default text & helperText"
            defaultValue="default Text"
            error={true}
            helperText="msg"
          />
          <TextField
            id="custom04"
            label="No helperText"
            defaultValue="default Text"
            error={true}
          />
          <TextField id="custom05" label="No default value" error={true} />
          <TextField
            id="custom06"
            label="Label "
            disabled={true}
            defaultValue="disabled Text"
            helperText="msg"
          />
        </Stack>
        <Stack spacing={12}>
          <Stack spacing={3} width="360px">
            <TextField
              id="07"
              label="Outlined sample 1"
              defaultValue="outlined TextField "
              variant="outlined"
            />
            <TextField
              id="08"
              label="Outlined sample 2 no defaultValue"
              variant="outlined"
            />
            <TextField
              id="09"
              label="Outlined sample 3"
              variant="outlined"
              helperText="some helperText"
            />
          </Stack>
        </Stack>
      </Stack>
    </main>
  );
}
