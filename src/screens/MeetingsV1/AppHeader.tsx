import React from "react";
import {
  AppBar,
  Box,
  Button,
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
  Typography,
} from "@mui/material";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";

declare module '@mui/material/Button' {
  interface ButtonPropsVariantOverrides {
    appHeader: true;
  }
}


export default function AppHeader() {
  const handleChange = (event: SelectChangeEvent) => {};

  return (
    <AppBar
      position="static"
      elevation={0}
      sx={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        height: "64px",
        background: "#FAFAFA",
        alignItems: "center",
        borderRadius: "10px 10px 0 0",
      }}
    >
      <Box sx={{ display: "flex", flexDirection: "row" }}>
        <Typography
          sx={{
            marginLeft: 2,
            color: "#000",
            fontWeight: "500",
            fontSize: "1.25rem",
          }}
        >
          Ooma
        </Typography>
        <Typography
          sx={{ marginLeft: "3px", color: "#000", fontSize: "1.25rem" }}
        >
          {" "}
          Meetings
        </Typography>
      </Box>
      <Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
        <TextField
          size="small"
          sx={{ width: "238px", marginRight: "16px" }}
          placeholder="Meeting ID"
        />
        <Button
          sx={{
            marginRight: "16px",
          }}
          variant="appHeader"
          startIcon={<CalendarTodayIcon />}
        >
          Meet Now
        </Button>
        <Button
          startIcon={<CalendarTodayIcon />}
          variant="appHeader"
        >
          Schedule
        </Button>
      </Box>
      <FormControl>
        <Select
          variant="standard"
          value={"Randy Oomazing"}
          onChange={handleChange}
          sx={{ width: "161px", height: "36px", marginRight: 2 }}
          disableUnderline
        >
          <MenuItem value={"Randy Oomazing"}>Randy Oomazing</MenuItem>
        </Select>
      </FormControl>
    </AppBar>
  );
}
