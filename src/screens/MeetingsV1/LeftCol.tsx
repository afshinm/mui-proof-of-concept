import React from "react";
import { Box, IconButton, Tab, Tabs, Typography } from "@mui/material";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import CachedIcon from "@mui/icons-material/Cached";

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ p: 2 }}>{children}</Box>}
    </div>
  );
}

function MyMeetingEntry({ showIcon = true }: { showIcon?: boolean }) {
  return (
    <Box sx={{ marginBottom: 1 }}>
      <Typography variant="subtitle1">My Personal Meeting</Typography>
      <Box
        sx={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          marginTop: showIcon ? "-8px" : "0px",
        }}
      >
        <Typography
          variant="caption"
          sx={{ marginRight: 2, color: "rgba(20, 20, 20, 0.72)" }}
        >
          ID: 3423423424
        </Typography>
        {showIcon && (
          <IconButton size="small">
            <CachedIcon />
          </IconButton>
        )}
      </Box>
    </Box>
  );
}

export default function LeftCol() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box
      component="section"
      sx={{
        width: "276px",
        background: "#FAFAFA",
        borderRadius: "0 0 0 10px ",
        overflow: "auto",
      }}
    >
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="meetings tab"
        variant="fullWidth"
      >
        <Tab label="My Meetings" sx={{ textTransform: "none" }} />
        <Tab label="Recents" sx={{ textTransform: "none" }} />
      </Tabs>

      <Box
        sx={{ display: "flex", justifyContent: "space-between", marginTop: 2 }}
      >
        <Typography sx={{ fontWeight: "500", ml: 2 }}>My Meetings</Typography>
        <Box sx={{ mr: 1, mt: "-8px" }}>
          <IconButton>
            <FilterAltIcon sx={{ color: "rgba(20, 20, 20, 0.54)" }} />
          </IconButton>
          <IconButton sx={{ ml: 1 }}>
            <KeyboardArrowUpIcon sx={{ color: "rgba(20, 20, 20, 0.54)" }} />
          </IconButton>
        </Box>
      </Box>
      <TabPanel value={value} index={0}>
        <MyMeetingEntry />
        <MyMeetingEntry />
        <MyMeetingEntry />
        <MyMeetingEntry showIcon={false} />
        <MyMeetingEntry showIcon={false} />
        <MyMeetingEntry showIcon={false} />
        <MyMeetingEntry />
        <MyMeetingEntry />
        <MyMeetingEntry />
      </TabPanel>
      <TabPanel value={value} index={1}>
        Item Two
      </TabPanel>
    </Box>
  );
}
