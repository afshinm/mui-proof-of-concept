import React from "react";
import { Box, Container, Divider } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import AppHeader from "./AppHeader";
import LeftCol from "./LeftCol";
import RightCol from "./RightCol";

const theme = createTheme({
  components: {
    MuiButton: {
      variants: [
        {
          props: { variant: "appHeader" },
          style: {
            width: "126px",
          },
        },
      ],
    },
  },
});

export default function MeetingsV1() {
  return (
    <ThemeProvider theme={theme}>
      <Container sx={{ maxWidth: "970px", borderRadius: "10px" }}>
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <AppHeader />
          <Divider />
          <Box
            component="main"
            sx={{ display: "flex", flexDirection: "row", height: "702px" }}
          >
            <LeftCol />
            <Divider orientation="vertical" />
            <RightCol />
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}
