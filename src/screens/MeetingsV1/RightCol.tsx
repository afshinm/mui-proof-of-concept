import React from "react";
import {
  Box,
  Button,
  Divider,
  IconButton,
  List,
  ListItemButton,
  ListSubheader,
  Typography,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import SettingsIcon from "@mui/icons-material/Settings";
import SvgIcon from '@mui/material/SvgIcon';

function RightColHeader() {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        mb: 1,
      }}
    >
      <Typography
        variant="subtitle1"
        sx={{ ml: 1, fontWeight: "500", color: "rgba(20, 20, 20, 0.94)" }}
      >
        Calendar
      </Typography>
      <Box sx={{ marginRight: 2 }}>
        <IconButton sx={{ marginRight: 2 }}>
          <SearchIcon sx={{ color: "#979797",}} />
        </IconButton>
        <IconButton>
          <SettingsIcon sx={{ color: "#979797" }} />
        </IconButton>
      </Box>
    </Box>
  );
}

function RightColListItem({
  now = false,
  title = "Another calendar entry",
  icon = false,
  conflict = false,
}) {
  const [over, setOver] = React.useState(false);

  return (
    <ListItemButton
      onMouseEnter={() => {
        setOver(true);
      }}
      onMouseLeave={() => {
        setOver(false);
      }}
    >
      <Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
        <Box sx={{ display: "flex", flexDirection: "column", marginRight: 5 }}>
          {now && (
            <Typography
              variant="caption"
              sx={{ color: "#ff9500", fontWeight: "500" }}
            >
              Now
            </Typography>
          )}
          {conflict && (
            <Typography
              variant="caption"
              sx={{ color: "#ff3b30", fontWeight: "500" }}
            >
              Conflict
            </Typography>
          )}
          <Typography variant="caption">11:30 AM</Typography>
          <Typography variant="caption">11:45 AM</Typography>
        </Box>
        <Divider orientation="vertical" flexItem />
        <Box sx={{ marginLeft: 2 }}>
          <Box
            sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}
          >
            {icon && (
              <SvgIcon width="16" height="16" viewBox="0 0 16 16" fill="none" sx={{width: '16px', height: '16px', mr: 1}}>
                <rect width="7.66667" height="7.66667" fill="#F25022" />
                <rect
                  x="8.33301"
                  width="7.66667"
                  height="7.66667"
                  fill="#7FBA00"
                />
                <rect
                  y="8.33325"
                  width="7.66667"
                  height="7.66667"
                  fill="#00A4EF"
                />
                <rect
                  x="8.33301"
                  y="8.33325"
                  width="7.66667"
                  height="7.66667"
                  fill="#FFB900"
                />
              </SvgIcon>
            )}
            <Typography variant="subtitle1">{title}</Typography>
          </Box>
          <Typography variant="caption" sx={{ color: "20, 20, 20, 0.72" }}>
            Meeting ID: 344544342
          </Typography>
        </Box>
        {over && (
          <Box sx={{ display: "relative" }}>
            <Button
              sx={{
                width: "55px",
                height: "30px",
                background: "rgba(63,81,181)",
                color: "white",
                borderRadius: "8px",
                textTransform: "none",
                // ml: 'auto',  // TODO: why wont this automagically flush the button to the right?
                position: "absolute", // this is a hack!
                left: "520px",
                top: "20px",
              }}
            >
              Start
            </Button>
          </Box>
        )}
      </Box>
    </ListItemButton>
  );
}

export default function RightCol() {
  // const [selectedIndex, setSelectedIndex] = React.useState(1);

  // const handleListItemClick = (
  //   event: React.MouseEvent<HTMLDivElement, MouseEvent>,
  //   index: number
  // ) => {
  //   setSelectedIndex(index);
  // };

  return (
    <Box
      sx={{
        flexGrow: 1,
        background: "#fff",
        borderRadius: "0 0 10px 0 ",
        p: 1,
        overflow: "auto",
      }}
    >
      <RightColHeader />
      <Divider />
      <List sx={{ width: "100%" }} component="nav">
        <ListSubheader>
          <Typography variant="h6" sx={{ color: "rgba(20,20,20,0.56)", mb: 1 }}>
            18 Jan
          </Typography>
          <Divider />
        </ListSubheader>

        <RightColListItem title="Ooma Enterprise - roadmap" icon={true} />
        <RightColListItem title="My Project - Daily Standup" />

        <ListSubheader>
          <Typography variant="h6" sx={{ color: "rgba(20,20,20,0.94)" }}>
            Today, 19 Jan
          </Typography>
          <Divider />
        </ListSubheader>

        <RightColListItem
          now={true}
          title="Spring Planning w/ web team"
          icon={true}
        />
        <RightColListItem title="Ooma Enterprise - roadmap" />
        <RightColListItem conflict={true} title="My Project - Daily Standup" />

        <ListSubheader>
          <Typography variant="h6" sx={{ color: "rgba(20,20,20,0.56)" }}>
            20 Jan
          </Typography>
          <Divider />
        </ListSubheader>

        <RightColListItem title="My Project - Daily Standup" />

        <ListSubheader>
          <Typography variant="h6" sx={{ color: "rgba(20,20,20,0.56)" }}>
            21 Jan
          </Typography>
          <Divider />
        </ListSubheader>
        <RightColListItem title="Ooma Enterprise - roadmap" icon={true} />
        <RightColListItem title="My Project - Daily Standup" />
        <RightColListItem title="Lunch & Learn" />
      </List>
    </Box>
  );
}
