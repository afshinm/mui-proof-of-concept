import React from "react";
import { Box, Button, Divider, Stack, Typography } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import SendIcon from "@mui/icons-material/Send";
import SplitButton from "./SplitButton.jsx";

export default function StackButtons({ whichTheme }: { whichTheme: string }) {
  return (
    <Stack m="50px" padding="10px" spacing={4}>
      <Typography variant="h3">{whichTheme} Buttons</Typography>
      <Typography variant="h6">Basic Buttons</Typography>

      <Stack spacing={2} direction="row">
        <Button variant="text" size="small">
          Text
        </Button>
        <Button variant="contained" color="primary" size="small">
          Contained
        </Button>
        <Button variant="outlined" size="small">
          Outlined
        </Button>
      </Stack>

      <Stack spacing={2} direction="row">
        <Button variant="text">Text</Button>
        <Button variant="contained" color="primary">
          Contained
        </Button>
        <Button variant="outlined">Outlined</Button>
      </Stack>

      <Stack spacing={2} direction="row">
        <Button variant="text" size="large">
          Text
        </Button>
        <Button variant="contained" color="primary" size="large">
          Contained
        </Button>
        <Button variant="outlined" size="large">
          Outlined
        </Button>
      </Stack>
      <Divider flexItem />
      <Typography>Disabled</Typography>
      <Stack spacing={2} direction="row">
        <Button disabled variant="text" size="small">
          Text
        </Button>
        <Button disabled variant="contained" color="primary" size="small">
          Contained
        </Button>
        <Button disabled variant="outlined" size="small">
          Outlined
        </Button>
      </Stack>

      <Stack spacing={2} direction="row">
        <Button disabled variant="text">
          Text
        </Button>
        <Button disabled variant="contained" color="primary">
          Contained
        </Button>
        <Button disabled variant="outlined">
          Outlined
        </Button>
      </Stack>

      <Stack spacing={2} direction="row">
        <Button disabled variant="text" size="large">
          Text
        </Button>
        <Button disabled variant="contained" color="primary" size="large">
          Contained
        </Button>
        <Button disabled variant="outlined" size="large">
          Outlined
        </Button>
      </Stack>

      <Divider flexItem />
      <Typography>With Icons</Typography>

      <Stack direction="row" spacing={2}>
        <Button variant="outlined" startIcon={<DeleteIcon />}>
          Delete
        </Button>
        <Button variant="contained" endIcon={<SendIcon />}>
          Send
        </Button>
        <Button
          variant="contained"
          startIcon={<SendIcon />}
          endIcon={<SendIcon />}
        >
          Send
        </Button>
      </Stack>
      <Stack direction="row" spacing={2}>
        <Button disabled variant="outlined" startIcon={<DeleteIcon />}>
          Delete
        </Button>
        <Button disabled variant="contained" endIcon={<SendIcon />}>
          Send
        </Button>
        <Button
          disabled
          variant="contained"
          startIcon={<SendIcon />}
          endIcon={<SendIcon />}
        >
          Send
        </Button>
      </Stack>

      <Divider flexItem />
      <Typography>Split Buttons</Typography>

      <SplitButton />
      <SplitButton variant="outlined" />
      <SplitButton variant="text" />
      <SplitButton disabled="true" />
      <SplitButton disabled="true" variant="outlined" />
      <SplitButton variant="text" disabled="true" />

      <Typography>
        There is no Split-Buttons in base Mui but the docs give an
        implementation under Button-Group.
      </Typography>
      <Typography>
        That implemenation wants to take the entire length of its container, so
        it has be wrapped in a containing div to act right:
      </Typography>

      <Box>
        <SplitButton />
      </Box>
    </Stack>
  );
}
