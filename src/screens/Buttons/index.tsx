import React from "react";
import { Divider, Stack } from "@mui/material";
import { ThemeProvider, createTheme } from "@mui/material/styles";

import StackButtons from "./StackButtons";

const innerTheme = createTheme({});

export default function Buttons() {
  return (
    <Stack
      direction="row"
      divider={<Divider orientation="vertical" flexItem />}
    >
      <StackButtons whichTheme="Brio" />

      <ThemeProvider theme={innerTheme}>
        <StackButtons whichTheme="MUI" />
      </ThemeProvider>
    </Stack>
  );
}
