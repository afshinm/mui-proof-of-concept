import React from "react";
import { Box, IconButton, Tab, Tabs, Typography } from "@mui/material";
import SibeBarList from "./SideBarList";
import CachedIcon from "@mui/icons-material/Cached";
import SearchIcon from "@mui/icons-material/Search";
import FilterAltIcon from "@mui/icons-material/FilterAlt";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const today = new Date();

export default function SideBar() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  return (
    <Box
      sx={{
        width: "36%", // ratio found by 346/948
        background: "#F9FCFF",
        borderRadius: "10px 0 10px 0",
      }}
    >
      <Box
        component="section"
        sx={{
          width: "100%",
          height: "56px",
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <Typography variant="subtitle1" sx={{ ml: 2 }}>
          Meetings
        </Typography>
        <Box>
          <IconButton aria-label="refresh">
            <CachedIcon />
          </IconButton>
          <IconButton aria-label="search">
            <SearchIcon />
          </IconButton>
          <IconButton aria-label="filter">
            <FilterAltIcon />
          </IconButton>
        </Box>
      </Box>

      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="tabs for upcoming, recent, and rooms"
        variant="fullWidth"
        role="navigation"
      >
        <Tab label="Upcoming" />
        <Tab label="Recents" disabled />
        <Tab label="Rooms" disabled />
      </Tabs>
      <Box sx={{ p: 1 }} >
        <Typography variant="subtitle1" sx={{ m: 1 }}>
          Today, {`${today.getDate()} ${monthNames[today.getMonth()]}`}
        </Typography>

        <SibeBarList />
      </Box>
    </Box>
  );
}
