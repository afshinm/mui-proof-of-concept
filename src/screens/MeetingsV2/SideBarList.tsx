import React from "react";
import {
  Box,
  Chip,
  Divider,
  List,
  ListItem,
  ListItemButton,
  SvgIcon,
  Typography,
} from "@mui/material";
import MtgCameraSvg from "./MtgCameraSvg";
import MtgLocationSvg from "./MtgLocationSvg";
import ErrorIcon from "@mui/icons-material/Error";

export default function SideBarList() {
  return (
    <List>
      <ListItem disablePadding>
        <ListItemButton
          sx={{
            borderRadius: "8px",
            mb: 1,
            backgroundColor: "rgba(219,235,243,1)", // TODO: Primary.Highlight
          }}
        >
          <Box>
            <Chip
              icon={<ErrorIcon />}
              size="small"
              label="Now"
              color="secondary"
            />
            <Typography
              variant="subtitle1"
              sx={{
                mt: 1,
                color: "primary.dark",
              }}
            >
              Standing Design Review Meeting
            </Typography>
            <Typography
              sx={{ mt: 1 }}
              variant="listingTimerange"
              color="primary"
            >
              11:30 AM - 11:45 AM • Host{" "}
            </Typography>
            <Box sx={{ display: "flex", alignItems: "center", mt: "14px" }}>
              <SvgIcon
                width="24"
                height="24"
                color="primary"
                viewBox="0 0 24 24"
                fill="none"
                sx={{ width: "16px" }}
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M4 5C1.79086 5 0 6.79086 0 9V15C0 17.2091 1.79086 19 4 19H12C14.2091 19 16 17.2091 16 15V9C16 6.79086 14.2091 5 12 5H4ZM18.6227 8.74631C18.2462 8.89971 18 9.26582 18 9.6724V14.3276C18 14.7342 18.2462 15.1003 18.6227 15.2537L22.0167 16.6364C22.9638 17.0223 24 16.3255 24 15.3029V8.69714C24 7.67447 22.9638 6.97772 22.0167 7.36357L18.6227 8.74631Z"
                />
              </SvgIcon>
              <Typography variant="caption" sx={{ ml: 1 }}>
                Ooma Meetings
              </Typography>
            </Box>
          </Box>
        </ListItemButton>

        <Divider />
      </ListItem>

      <ListItem disablePadding>
        <ListItemButton>
          <Box>
            <Typography
              variant="subtitle1"
              sx={{
                color: "primary.dark",
              }}
            >
              OE Product Sync
            </Typography>
            <Typography
              sx={{
                mt: 1,
                color: "rgba(20,20,20,0.56)", //TODO text.tertiary
              }}
              variant="listingTimerange"
            >
              12:30 PM - 1:00 PM
            </Typography>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                mt: "14px",
                mb: 2,
              }}
            >
              <MtgCameraSvg />
              <Typography
                variant="caption"
                sx={{
                  ml: 1,
                  color: "action.active",
                }}
              >
                Zoom
              </Typography>
            </Box>
          </Box>
        </ListItemButton>
        <Divider />
      </ListItem>

      <ListItem disablePadding>
        <ListItemButton>
          <Box>
            <Typography
              variant="subtitle1"
              sx={{
                color: "primary.dark",
              }}
            >
              Meetings / Brio - WIP
            </Typography>
            <Typography
              sx={{
                mt: 1,
                color: "rgba(20,20,20,0.56)", // TODO text.tertiary
              }}
              variant="listingTimerange"
            >
              2:00 PM - 3:30 PM
            </Typography>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                mt: "14px",
                mb: 2,
              }}
            >
              <MtgLocationSvg />
              <Typography
                variant="caption"
                sx={{ ml: 1, color: "action.active" }}
              >
                Office #3
              </Typography>
            </Box>
          </Box>
        </ListItemButton>
      </ListItem>
      <Divider />
      <ListItem disablePadding>
        <ListItemButton>
          <Box>
            <Typography
              variant="subtitle1"
              sx={{
                color: "primary.dark",
              }}
            >
              UX-warriors weekly
            </Typography>
            <Typography
              sx={{ mt: 1, color: "rgba(20,20,20,0.56)" }}
              variant="listingTimerange"
            >
              5:00 PM - 5:30 PM
            </Typography>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                mt: "14px",
                mb: 2,
              }}
            >
              <MtgCameraSvg />
              <Typography
                variant="caption"
                sx={{ ml: 1, color: "rgba(20, 20, 20, 0.54)" }}
              >
                Ooma Meetings
              </Typography>
            </Box>
          </Box>
        </ListItemButton>
      </ListItem>
      <Divider />
      <ListItem disablePadding>
        <ListItemButton>
          <Box>
            <Typography
              variant="subtitle1"
              sx={{
                color: "primary.dark",
              }}
            >
              20 Jan
            </Typography>
            <Typography
              sx={{
                mt: 1,
                color: "rgba(20,20,20,0.56)", // TODO: text.tertiary
              }}
              variant="listingTimerange"
            >
              11:30 AM - 11:45 AM
            </Typography>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                mt: "14px",
                mb: 2,
              }}
            >
              <MtgCameraSvg />
              <Typography
                variant="caption"
                sx={{ ml: 1, color: "action.active" }}
              >
                Zoom
              </Typography>
            </Box>
          </Box>
        </ListItemButton>
      </ListItem>
      <Divider />
      <Typography sx={{ mt: "30px", textAlign: "center" }}>
        Load more events
      </Typography>
    </List>
  );
}
