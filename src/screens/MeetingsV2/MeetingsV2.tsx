import React from "react";
import { Box, Container, Divider } from "@mui/material";
import SideBar from "./SideBar"
import PrimaryContent from "./PrimaryContent"

export default function MeetingsV2() {
  return (
    <Container
      disableGutters={true} // this triggers customizations in ../index.tsx
      // fixed={true}       // if true then container will snap to breakpoints
      // maxWdith={false}   // typescript hates this but the docs have it!
      // sx={{ width: '948px', maxHeight: '780px' }}
    >
      <Box
        sx={{
          width: "100%",
          minHeight: "500px",
          backgroundColor: "white",
          borderRadius: "10px",
          display: "flex",
        }}
      >
        <SideBar />
        <Divider orientation="vertical" flexItem />
        <PrimaryContent />
      </Box>
    </Container>
  );
}
