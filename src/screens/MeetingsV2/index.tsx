import React from "react";
import { createTheme, Theme, ThemeProvider } from "@mui/material/styles";
import MeetingsV2 from "./MeetingsV2";
import { meetingsColors as colors } from "./colors";
// import { color } from "@mui/system";

declare module "@mui/material/styles" {
  interface Palette {
    primaryContainer?: React.CSSProperties["color"];
  }
  interface PaletteOptions {
    primaryContainer?: React.CSSProperties["color"];
  }

  interface TypographyVariants {
    listingTimerange: React.CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    listingTimerange?: React.CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module "@mui/material/Typography" {
  interface TypographyPropsVariantOverrides {
    listingTimerange: true;
  }
}

// An example of customizing the app-wide theme...
// here we're creating a screen-wide theme on top of the app-wide, Brio, & Mui
export default function MeetingsV2Wrapper() {
  return (
    <ThemeProvider
      theme={(theme: Theme) => {
        const t = createTheme({
          ...theme,
          palette: {
            ...theme.palette,
            primary: {
              main: colors.PRIMARY_MAIN,
              light: colors.PRIMARY_LIGHT,
              dark: colors.PRIMARY_DARK,
              contrastText: colors.PRIMARY_CONTRAST,
              // TODO: add highlight?
            },
            secondary: {
              main: colors.SECONDARY_MAIN,
              light: colors.SECONDARY_LIGHT,
              dark: colors.SECONDARY_DARK,
              contrastText: colors.SECONDARY_CONTRAST,
            },
            primaryContainer: colors.PRIMARY_CONTAINER,
          },
          typography: {
            ...theme.typography,
            h5: {
              fontSize: theme.typography.pxToRem(24),
              lineHeight: theme.typography.pxToRem(32),
              letterSpacing: "-0.5px",
            },
            subtitle1: {
              fontWeight: 500,
              fontSize: theme.typography.pxToRem(16),
              lineHeight: theme.typography.pxToRem(20),
              letterSpacing: "-0.3px",
              color: theme.palette.text.primary,
            },
            subtitle2: {
              fontWeight: 400,
              fontSize: theme.typography.pxToRem(14),
              lineHeight: theme.typography.pxToRem(20),
              letterSpacing: "0.1px",
              color: theme.palette.text.secondary,
            },
            caption: {
              fontSize: theme.typography.pxToRem(12),
              lineHeight: theme.typography.pxToRem(16),
            },
            // custom variant
            listingTimerange: {
              fontSize: theme.typography.pxToRem(12),
              lineHeight: theme.typography.pxToRem(16),
              fontWeight: theme.typography.fontWeightBold,
            },
          },
          components: {
            ...theme.components,
            MuiContainer: {
              styleOverrides: {
                disableGutters: {
                  height: "100vh",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                },
              },
            },
            MuiIconButton: {
              defaultProps: {
                size: "large",
                color: "primary",
              },
            },
            MuiTab: {
              styleOverrides: {
                root: {
                  textTransform: "none",
                  fontSize: "14px",
                  lineHeight: "20px",
                  letterSpacing: "-.25px",
                  fontWeight: "bold",
                  borderColor: theme.palette.divider, // TODO
                  borderBottom: "1px solid",
                },
              },
            },
            MuiListItemButton: {
              styleOverrides: {
                // over-riding 'gutters' wasn't as useful
                root: {
                  paddingLeft: "8px",
                  paddingTop: "14px",
                },
              },
            },
            MuiFilledInput: {
              styleOverrides: {
                input: {
                  paddingTop: "8px",
                  background: colors.PRIMARY_MAIN,
                  color: colors.PRIMARY_CONTRAST,
                  "& .MuiSvgIcon-root": { color: colors.PRIMARY_CONTRAST },
                  fontWeight: theme.typography.fontWeightMedium,
                  '& :hover': {
                    background: colors.PRIMARY_DARK,
                  },
                },
              },
            },
            MuiLink: {
              styleOverrides: {
                root: {
                  fontSize: theme.typography.pxToRem(16),
                  lineHeight: theme.typography.pxToRem(24),
                  letterSpacing: "-0.5px",
                  color: colors.SECONDARY_MAIN,
                },
              },
            },
            MuiSelect: {
              styleOverrides: {
                filled: {
                  borderRadius: '8px',
                },
              },
            },
            
          },
        });
        console.log(t);
        return t;
      }}
    >
      <MeetingsV2 />
    </ThemeProvider>
  );
}
