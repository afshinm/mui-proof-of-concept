import React from "react";
import {
  Box,
  Button,
  Chip,
  Divider,
  FormControl,
  IconButton,
  // InputLabel,
  Link,
  MenuItem,
  SvgIcon,
  // TextField,
  Typography,
} from "@mui/material";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import ErrorIcon from "@mui/icons-material/Error";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";

import { alpha, styled } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import { Theme } from "@mui/material/styles";

const BootstrapInput = styled(InputBase)(({ theme }: { theme: Theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    backgroundColor: theme.palette.action.hover,
    borderRadius: "8px 0 0 8px",
    position: "relative",
    border: "none",
    height: "16px",
    padding: "10px 12px",
    transition: theme.transitions.create([
      "border-color",
      "background-color",
      "box-shadow",
    ]),
    "&:focus": {
      boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
    },
    [theme.breakpoints.up("md")]: {
      width: "195px",
    },
  },
}));

function MeetingIDandJoinInHeader() {
  return (
    /* <TextField
        size="small"
        sx={{ "& .MuiInputBase-root": { borderRadius: "8px" } }}
      /> */
    <Box sx={{ display: "flex" }}>
      <FormControl>
        {/* <InputLabel  shrink htmlFor="bootstrap-input" margin="dense">
          Meeting ID
        </InputLabel> */}
        <BootstrapInput
          placeholder="Meeting ID"
          id="bootstrap-input"
          size="small"
        />
      </FormControl>
      <Button sx={{ borderRadius: "0 8px 8px 0" }}>Join</Button>
    </Box>
  );
}

function HeaderAppBar() {
  const [selectVal, setSelectVal] = React.useState("10");

  const handleChange = (event: SelectChangeEvent) => {
    setSelectVal(event.target.value as string);
  };
  return (
    <Box
      sx={{
        width: "100%",
        height: "56px",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        px: 2,
      }}
    >
      <MeetingIDandJoinInHeader />
      <Button
        variant="contained"
        startIcon={<CalendarTodayIcon />}
        sx={{ ml: 2, width: "124px" }}
      >
        Schedule
      </Button>
      <FormControl
        variant="filled"
        sx={{ ml: 2, width: "155px", maxWidth: "155px" }}
      >
        {/* <InputLabel disableAnimation margin="dense" id="Meet now select">
          Meet now
        </InputLabel> */}
        <Select
          disableUnderline
          label="Meet now"
          sx={
            {
              // "& .MuiSvgIcon-root": { color: "white" },
              // '& .MuiFilledInput-input': {
              //   backgroundColor: 'primary.main',
              //   borderRadius: "8px",
              // },
              // "& :hover": {
              //   backgroundColor: "primary.dark",
              //   borderRadius: "8px",
              // },
            }
          }
          value={selectVal}
          onChange={handleChange}
        >
          <MenuItem value={10}>Meet Now</MenuItem>
          <MenuItem value={20}>Option 2</MenuItem>
        </Select>
      </FormControl>
    </Box>
  );
}

function NowButtonRow() {
  return (
    <Box sx={{ mt: 2 }}>
      <Button>Start Meeting</Button>
      <Button variant="outlined" sx={{ ml: 2 }}>
        View in Outlook
      </Button>
      <IconButton sx={{ ml: 2 }} aria-label="more options">
        <MoreVertIcon />
      </IconButton>
    </Box>
  );
}

// Over-ride the border-radius <TextField> customization set in Brio
export default function PrimaryContent() {
  return (
    <Box component="main" sx={{ flex: 1 }}>
      <HeaderAppBar />
      <Divider />
      <Box component="section" sx={{ px: 2, mt: "14px" }}>
        <Chip icon={<ErrorIcon />} size="small" label="Now" color="secondary" />
        <Typography
          variant="h5"
          sx={{
            mt: "10px",
            color: "primary.dark",
          }}
        >
          Standing Design Review Meeting
        </Typography>
        <Typography variant="subtitle2">
          Today, 19 Jan, 11:30 AM - 11:45 AM
        </Typography>
        <Box sx={{ display: "flex", alignItems: "center", mt: 1 }}>
          <SvgIcon
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
            sx={{
              width: "16px",
              color: "rgba(216, 216, 216, 1)", //  TODO
            }}
          >
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M4 5C1.79086 5 0 6.79086 0 9V15C0 17.2091 1.79086 19 4 19H12C14.2091 19 16 17.2091 16 15V9C16 6.79086 14.2091 5 12 5H4ZM18.6227 8.74631C18.2462 8.89971 18 9.26582 18 9.6724V14.3276C18 14.7342 18.2462 15.1003 18.6227 15.2537L22.0167 16.6364C22.9638 17.0223 24 16.3255 24 15.3029V8.69714C24 7.67447 22.9638 6.97772 22.0167 7.36357L18.6227 8.74631Z"
            />
          </SvgIcon>
          <Typography variant="subtitle2" sx={{ ml: 1 }}>
            Ooma Meetings
          </Typography>
        </Box>
        <NowButtonRow />
        <Divider sx={{ mt: 2 }} />
        <Typography variant="subtitle1" sx={{ mt: 2 }}>
          Information
        </Typography>
        <Typography variant="caption" sx={{ mt: 1 }}>
          Host
        </Typography>
        <Typography sx={{}}>You</Typography>
        <Typography variant="caption" sx={{ mt: 1 }}>
          Attendees
        </Typography>
        <Typography sx={{}}>Rita Law, Natalie Chen, Jag Sekhon</Typography>
        <Typography variant="caption" sx={{ mt: 1 }}>
          Location
        </Typography>
        <Box>
          <Link>https: //meetings.ooma.com/9132244332</Link>
          <ContentCopyIcon
            sx={{
              ml: 1,
              color: "action.active",
            }}
          />
        </Box>
        <Typography variant="caption" sx={{ mt: 1 }}>
          Password
        </Typography>
        <Typography sx={{}}>sdkaDSdfj23#k</Typography>
        <Typography variant="subtitle1" sx={{ mt: "24px" }}>
          Description
        </Typography>
        <Typography sx={{ mt: 1 }}>
          Luda Shashua is inviting you to an Ooma Meeting
        </Typography>
      </Box>
    </Box>
  );
}
