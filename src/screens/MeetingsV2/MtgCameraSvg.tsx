import React from "react";
import { SvgIcon } from "@mui/material";

export default function MtgCameraSvg() {
  return (
    <SvgIcon
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      sx={{
        width: "16px",
        color: "#979797", // TODO:
      }}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4 5C1.79086 5 0 6.79086 0 9V15C0 17.2091 1.79086 19 4 19H12C14.2091 19 16 17.2091 16 15V9C16 6.79086 14.2091 5 12 5H4ZM18.6227 8.74631C18.2462 8.89971 18 9.26582 18 9.6724V14.3276C18 14.7342 18.2462 15.1003 18.6227 15.2537L22.0167 16.6364C22.9638 17.0223 24 16.3255 24 15.3029V8.69714C24 7.67447 22.9638 6.97772 22.0167 7.36357L18.6227 8.74631Z"
      />
    </SvgIcon>
  );
}
