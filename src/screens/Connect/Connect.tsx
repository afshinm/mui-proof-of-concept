import React from "react";
import {
  Box,
  Button,
  Card,
  // Container,
  Divider,
  FormControl,
  InputLabel,
  Input,
  FormHelperText,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import HeaderWithLogo from "../../shared/HeaderWithLogo";
import WarningIcon from "@mui/icons-material/Warning";

type OomaTextInputProps = {
  label: string;
  helperText?: string;
  inputId: string;
  error?: boolean;
  // All other props
  // [x: string]: any;
};
function OomaTextInput({
  label,
  helperText='',
  inputId,
  error = false,
}: OomaTextInputProps) {
  return (
    <FormControl>
      <InputLabel
        htmlFor={inputId}
        disableAnimation={true}
        shrink={true}
        color="secondary"
        sx={{
          mt: "-9px",
          ml: "-12px",
          fontSize: "14px",
          lineHeight: "20px",
          letterSpacing: "-.25 px",
          color: "text.secondary",
        }}
      >
        {label}
      </InputLabel>
      <Input
        id={inputId}
        aria-describedby={`${inputId}-helper-text`}
        disableUnderline
        error={error}
        sx={{
          border: "2px solid #5D6FD4",
          borderRadius: "8px",
          height: "42px",
          lineHeight: '24px',
          pl: '12px',
        }}
      />
      <Stack direction="row" mt="2px">
        {error && <WarningIcon color="error" sx={{ mr: 1 }} />}
        <FormHelperText
          id={`${inputId}-helper-text`}
          error={error}
          sx={{
            ml: 0,
            fontSize: "12px",
            lineHeight: "16px",
            color: "text.secondary",
          }}
        >
          {helperText}
        </FormHelperText>
      </Stack>
    </FormControl>
  );
}

export default function Connect() {
  return (
    <Stack
      width="100%"
      height="100vh"
      bgcolor="graphite1"
      alignItems="center"
      sx={{ overflow: "hidden" }}
    >
      <HeaderWithLogo />
      <Card
        component="main" // tells component what html element to use instead of a div
        // raised  // is by default true as per app theme customizations
        sx={{
          mt: 24,
          maxWidth: "470px",
          minHeight: "334px",
          // common <Card> props below this line put on all Cards
          // display: "flex",
          // flexDirection: "column",
          // width: "100%",
        }}
      >
        <Stack
          width="100%"
          height="56px"
          sx={{
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Typography variant="h6" sx={{ letterSpacing: ".15px" }}>
            Connect Account
          </Typography>
        </Stack>
        <Divider />
        <Typography sx={{ mx: 2, mt: 2, letterSpacing: "-0.5px" }}>
          Please type an email associated with your Google or Microsoft account.
        </Typography>
        <Box sx={{ mx: 2 }}>
          <TextField
            required
            // variant="filled"  // this is made default by Brio theme
            sx={{
              my: "24px",
              width: "100%",
            }}
            label="Email"
          />
          {/* <TextField
            sx={{ mt: 3 }}
            variant="filled"
            label="the label"
            size="small"
          /> */}
          <OomaTextInput
            label="El labelo"
            helperText="El erroro"
            inputId="test-inpt"
            // error={true}
          />
        </Box>
        <Button
          sx={{
            mt: "auto",
            mr: 2,
            mb: "20px",
            width: "94px",
            alignSelf: "end",
          }}
        >
          Continue
        </Button>
      </Card>
    </Stack>
  );
}
