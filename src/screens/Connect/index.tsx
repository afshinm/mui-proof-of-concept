import React from "react";
import { createTheme, Theme, ThemeProvider } from "@mui/material/styles";
import Connect from "./Connect";

export default function ConnectWrapper() {
  return (
    <ThemeProvider
      theme={(theme: Theme) => {
        return createTheme({
          ...theme,
          palette: {
            ...theme.palette,
            primary: { main: "rgba(69, 90, 100, 1)" },  // the green
            text: {
              secondary: 'rgba(0, 0, 0, 0.6)',
            },
          },

        });
      }}
    >
      <Connect />
    </ThemeProvider>
  );
}
