import React from "react";
import { createTheme, Theme, ThemeProvider } from "@mui/material/styles";
import Thankyou from "./Thankyou";

export default function ThankyouWrapper() {
  return (
    <ThemeProvider
      theme={(theme: Theme) => {
        return createTheme({
          ...theme,
          palette: {
            ...theme.palette,
            primary: { main: "rgba(69, 90, 100, 1)" }, // the green
          },
        });
      }}
    >
      <Thankyou />
    </ThemeProvider>
  );
}
