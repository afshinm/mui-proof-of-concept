import React from "react";
import {
  Box,
  Button,
  Card,
  Typography,
} from "@mui/material";
import HeaderWithLogo from "../../shared/HeaderWithLogo";
import CheckIcon from "@mui/icons-material/Check";

export default function Thankyou() {
  return (
    <Box
      sx={{
        width: "100%",
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: 'graphite1',
        overflow: "hidden", // for scrollbar caused by header
      }}
    >
      <HeaderWithLogo />
      <Card
        component="main"
        // raised // is by default true as per app theme customizations
        sx={{
          mt: 4,
          maxWidth: "712px",
          minHeight: "336px",
          alignItems: "center",
          // common <Card> props below this line
          // display: "flex",
          // flexDirection: "column",
          // width: "100%",
        }}
      >
        <CheckIcon color="success" sx={{ fontSize: "41px", mt: 6 }} />
        <Typography variant="h4" sx={{ lineHeight: "48px", mt: 4 }}>
          Thank You
        </Typography>
        <Typography variant="h6" sx={{ letterSpacing: ".15px", fontWeight: '400', lineHeight: '24px', mt:1 }}>
          Your Google account is now connected
        </Typography>
        <Button
          sx={{
            mt: 3,
            // width: "94px",
          }}
        >
          Go back to the app
        </Button>
      </Card>
    </Box>
  );
}
