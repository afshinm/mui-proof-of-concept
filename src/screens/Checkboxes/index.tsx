import { Checkbox, Divider, Stack, Typography } from "@mui/material";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";

const label = { inputProps: { "aria-label": "Checkbox demo" } };

export default function StackButtons({ whichTheme }: {whichTheme:string}) {
  return (
    <Stack m="50px" padding="10px" spacing={4}>
      <Typography variant="h3">{whichTheme} Checkboxes</Typography>
      <Typography variant="h6">Basic Checkboxes</Typography>

      <Stack spacing={2} direction="row">
        <Checkbox {...label} defaultChecked />
        <Checkbox {...label} />
        <Checkbox {...label} disabled />
        <Checkbox {...label} disabled checked />
      </Stack>

      <Divider flexItem />

      <Stack direction="row" spacing={2}>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox defaultChecked />}
            label="Label"
          />
          <FormControlLabel disabled control={<Checkbox />} label="Disabled" />
        </FormGroup>
      </Stack>
    </Stack>
  );
}
