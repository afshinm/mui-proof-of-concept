/* Responsive Components 
    - with Breakpoints 

xs, extra-small: 0px
sm, small: 600px
md, medium: 900px
lg, large: 1200px
xl, extra-large: 1536px
These values can be customized.
*/

import { styled } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import { green } from '@mui/material/colors';


// Mui provides media query helpers
const Wrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(1),
  [theme.breakpoints.down('sm')]: {
    backgroundColor: theme.palette.secondary.main,
  },
  [theme.breakpoints.up('md')]: {
    backgroundColor: theme.palette.primary.main,
  },
  [theme.breakpoints.up('lg')]: {
    backgroundColor: green[500],
  },
}));


// Here is one way we can show different colored text based on screen width
export default function MediaQuery() {
  return (
    <Wrapper>
      <Typography>My background will depend on screen width</Typography>
    </Wrapper>
  );
}