import React from "react";
import { Box, Button, Card, Link } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import { visuallyHidden } from '@mui/utils';




/*
  MUI min requirements

- React, starting with ^17.0.0
- MUI requires a minimum version of TypeScript 3.5;vyou can use JS
- Browsers : latest stable releases of Edge, Firefox, Chrome, Safari
	- IE partial support,  MUI v6 will not support IE
*/




// Common UI elements with common props and variations
const MyComponent = () => {
  <Button variant="contained">Log in</Button>;
};

const MyComponent2 = () => {
  <Button color="success" size="large" endIcon={<SearchIcon />}>
    Lets go
  </Button>;
};




/* Most margins are multiples of spacing scale; 
   in the code you can:
*/
   <Card sx={{ 
     mt: 3,         // shorthand for margin-top: 3 * 8px = 24px
    }}>text</Card>




// Improving accessibility with screen readers
export default function VisuallyHiddenUsage() {
  return (
    <Link href="#foo">
      Read more
      {/* always visually hidden because the parent is focusable element */}
      <Box sx={visuallyHidden}>this text available to screen readers</Box>
    </Link>
  );
}


