import React from "react";
import {
  Box,
} from "@mui/material";
import OomaIcon from "./OomaIcon1";

export default function HeaderWithLogo() {
  // todo: make this generic for white-labeling purposes
  return (
    <Box
      component="header"
      sx={{
        height: "64px",
        width: "100%",
        maxWidth: "1230px", // 1198 + ( 16px x 2 )
        display: "flex",
        alignItems: "center",
        ml: 4,
      }}
    >
      <OomaIcon />
    </Box>
  );
};