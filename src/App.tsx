import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Container, Divider, Typography } from "@mui/material";
import { List, ListItem, ListItemButton } from "@mui/material";
import MeetingsV1 from "./screens/MeetingsV1";
import MeetingsV2 from "./screens/MeetingsV2";
import Connect from "./screens/Connect";
import Thankyou from "./screens/Thankyou";
import TextInputScreen from "./screens/TextInput";
import Blankpage from "./screens/Blankpage";
import Buttons from "./screens/Buttons";
import Checkboxes from "./screens/Checkboxes";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/checkboxes">
          <Checkboxes whichTheme="brio" />
        </Route>
        <Route path="/buttons">
          <Buttons whichTheme="brio" />
        </Route>
        <Route path="/blankpage">
          <Blankpage />
        </Route>
        <Route path="/textinput">
          <TextInputScreen />
        </Route>
        <Route path="/meetings1">
          <MeetingsV1 />
        </Route>
        <Route path="/meetings2">
          <MeetingsV2 />
        </Route>
        <Route path="/connect">
          <Connect />
        </Route>
        <Route path="/thankyou">
          <Thankyou />
        </Route>
        <Route path="/">
          <Container>
            <Typography component="h1" variant="h3" sx={{ mt: 3 }}>
              Brio Playground
            </Typography>
            <nav aria-label="navigation to different screens">
              <List>
                <ListItem>
                  <ListItemButton>
                    <Link to="/meetings1">
                      <Typography variant="h4">
                        Ooma Meetings Calendar v1 POC
                      </Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>
                <ListItem>
                  <ListItemButton>
                    <Link to="/meetings2">
                      <Typography variant="h4">
                        Ooma Meetings Calendar v2
                      </Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>
                <ListItem>
                  <ListItemButton>
                    <Link to="/connect">
                      <Typography variant="h4">
                        Integrations Server - Connect
                      </Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>
                <ListItem>
                  <ListItemButton>
                    <Link to="/thankyou">
                      <Typography variant="h4">
                        Integrations Server - Connect Confirmation
                      </Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>
                <ListItem>
                  <ListItemButton>
                    <Link to="/textinput">
                      <Typography variant="h4">Custom TextInput</Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>

                <ListItem>
                  <ListItemButton>
                    <Link to="/blankpage">
                      <Typography variant="h4">
                        Blank page with Ooma logo in header
                      </Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>

                <ListItem>
                  <ListItemButton>
                    <Link to="/buttons">
                      <Typography variant="h4">Buttons</Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>

                <ListItem>
                  <ListItemButton>
                    <Link to="/checkboxes">
                      <Typography variant="h4">Checkboxes</Typography>
                    </Link>
                  </ListItemButton>
                </ListItem>
              </List>
            </nav>
            <Divider />
            <main>
              <Typography variant="h4" sx={{ mt: 3 }}>
                The code:
              </Typography>
              <Typography variant="h6" sx={{ mt: 2 }}>
                <code>/index.css</code> - pulls Ooma Untitled Sans font in,
              </Typography>
              <Typography variant="h6">
                <code>/index.tsx</code> - wraps app in theme provider and pulls
                in apps theme from theme directory,
              </Typography>
              <Typography variant="h6">
                <code>/tsconfig.json</code> - has references to Brio types def
                file (HELP?),
              </Typography>
              <Typography variant="h6" sx={{ mt: 1 }}>
                <code>/theme</code> - Brio is in here, as well as apps own mui
                customizations,
              </Typography>
              <Typography variant="h6">
                <code>/theme/index.ts</code> - Customizations to Mui for whole
                app on top of Brio, or just passes Brio as theme.
              </Typography>
              <Typography variant="h6">
                <code>/theme/theme.d.ts</code> - Some customizations will
                requires appending Mui's type definitions, which will go here.
              </Typography>
              <Typography variant="h6">
                <code>/theme/brio</code> - Brio customizations to MUI
              </Typography>
              <Typography variant="h6" sx={{ mt: 1 }}>
                <code>/theme/brio/index.ts</code> - Customizations to Mui; pulls
                in all the different components specific customizations
              </Typography>
              <Typography variant="h6">
                <code>/theme/brio/core.ts</code> - Core customizations to Mui
                pulled into brio/index.ts above
              </Typography>
              <Typography variant="h6">
                <code>/theme/brio/version.xxx.txt</code> - Brio theme version.
                I'll increment this as I iterate. (HELP?)
              </Typography>
              <Typography variant="h6">
                <code>/theme/brio/components/</code> - Directory contains a file
                for each customized Mui component
              </Typography>
            </main>
          </Container>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
