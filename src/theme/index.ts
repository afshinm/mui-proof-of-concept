// import { createTheme } from '@mui/material/styles';
import theme from './brio';

// If you don't have any app-wide customizations, or don't want to
// make those customizations via tweaking the theme here, then just
// export Brio's customizations by uncommenting next line:
export default theme;

// else, app-wide theme customizations below:
/*
export default createTheme(theme, {
  components: {
    // MuiButton,
    // MuiTextField,
    MuiCard : {
      defaultProps: {
        raised: true,
      },
      styleOverrides: {
        // All cards in this app will have the properties applied to root here:
        // You do this when you find patterns that keep repeating in this app.
        root: {                 
          display: "flex",
          flexDirection: "column",          
          width: "100%",
        },
      },
    },
    // ...
}});
*/
