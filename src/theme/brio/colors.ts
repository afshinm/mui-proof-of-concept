export const brioColors = {
  BKGD_DEFAULT: 'rgba(250, 250, 250, 1)', // #FAFAFA
  BKGD_PAPER: 'rgba(255, 255, 255, 1)',   // #FFF

  TEXT_PRIMARY: 'rgba(0, 0, 0, 0.87)',    // #000 @ 87%
  TEXT_SECONDARY: 'rgba(0, 0, 0,.60)',   // #000 @ 60%
  TEXT_TERTIARY: 'rgba(0, 0, 0,.56)',    // #000 @ 56%
  TEXT_DISABLED: 'rgba(0, 0, 0,.32)',    // #000 @ 32%

  PRIMARY_MAIN: "rgba(46, 46, 46, 1)",   // #2E2E2E
  PRIMARY_LIGHT: 'rgba(73, 73, 73, 1)', // #494949
  PRIMARY_DARK: 'rgba(31, 31, 31, 1)',     // #1F1F1F

  SECONDARY_MAIN: 'rgba(8, 115, 237, 1)',  // #0873ED
  SECONDARY_LIGHT: 'rgba(70, 143, 255, 1)',// #468FFF
  SECONDARY_DARK: 'rgba(0, 68, 149, 1)',  // #004495

  ERROR_MAIN: 'rgba(221, 55, 48, 1)',     // #DD3730
  ERROR_LIGHT: 'rgba(255, 137, 122, 1)',  // #FF897A
  ERROR_DARK: 'rgba(104, 0, 3, 1)',       // #680003
  // ERROR_CONTAINER: 'rba(253, 244, 242, 1)',//#FFDAD4  ??

  WARNING_MAIN: 'rgba(250, 145, 0, 1)',    // #FA9100
  WARNING_LIGHT: 'rgba(255, 184, 113, 1)', // #FFB871
  WARNING_DARK: 'rgba(141, 79, 0, 1)',     // #8D4F00

  INFO_MAIN: 'rgba(8, 115, 237, 1)',      // #0873ED
  INFO_LIGHT: 'rgba(70, 143, 255, 1)',    // #468FFF
  INFO_DARK: 'rgba(0, 68, 149, 1)',       // #004495

  SUCCESS_MAIN: 'rgba(47, 196, 86, 1)',      // #2FC456
  SUCCESS_LIGHT: 'rgba(83, 225, 111, 1)',    // #53E16F
  SUCCESS_DARK: 'rgba(0, 110, 37, 1)',       // #006E25

  ACTION_ACTIVE: 'rgba(0, 0, 0, .54)',    // #000 @ 54 %
  ACTION_HOVER: 'rgba(0, 0, 0, .04)',     // #000 @ 4 %
  ACTION_SELECTED: 'rgba(0, 0, 0, .08)',  // #000 @ 8%
  ACTION_FOCUS: 'rgba(0, 0, 0, .12)',     // #000 @ 12 %
  ACTION_DISABLED: 'rgba(0, 0, 0, .26)',  // #000 @ 26 %
  ACTION_DISABLED_BG: 'rgba(0, 0, 0,.12)',// #000 @ 12 %

  DIVIDER: 'rgba(0, 0, 0, 0.12)',        // #000 @ 12%
  BACKDROP: 'rgba(0, 0, 0, 0.56)',       // #000 @56%
  INPUT: 'rgba(0, 0, 0, 0.08)',          // #000 @8%


  /* TODO:
    How are these colors used?
    if we want something like:
      <Button color="neutral" >
    then they should probably be added under palette,
      (last example here: https://mui.com/customization/palette/#adding-new-colors)
    else add them on same level as palette?
      (as above, how 'status' is implemented there) 
  */
  GRAPHITE1: 'rgba(241, 241, 241, 1)',    // #F1F1F1
  GRAPHITE2: 'rgba(222, 222, 222, 1)',    // #DEDEDE
  GRAPHITE3: 'rgba(161, 161, 161, 1)',    // #A1A1A1
  GRAPHITE4: 'rgba(122, 122, 122, 1)',    // #7A7A7A
  GRAPHITE5: 'rgba(97, 97, 97, 1)',       // #616161
  GRAPHITE6: 'rgba(71, 71, 71, 1)',       // #474747
  GRAPHITE7: 'rgba(33, 33, 33, 1)',       // #212121
  GRAPHITE8: 'rgba(0, 0, 0, 1)',          // #000
};
