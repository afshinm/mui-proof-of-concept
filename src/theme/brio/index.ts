import { createTheme } from '@mui/material/styles';
import {
  MuiAppBar,
  MuiButton,
  MuiButtonGroup,
  MuiFilledInput,
  MuiTextField,
  MuiCard,
  MuiFormControl,
  MuiSelect,
  MuiCheckbox,
  // MuiFormControl,
  // MuiInputBase,
  // MuiInputLabel,
  // ...
} from "./components";

import theme from './core';

export default createTheme(theme, {
  components: {
    MuiAppBar,
    MuiButton,
    MuiButtonGroup,
    MuiFilledInput,
    MuiTextField,
    MuiCard,
    MuiFormControl,
    MuiSelect,
    MuiCheckbox,
    // MuiFormControl,
    // MuiInputLabel,
    // ...
}});
