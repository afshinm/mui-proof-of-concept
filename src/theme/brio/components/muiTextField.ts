import theme from "../core";

/* Brio custom textfield;  see also muiFormControl customizations
 */

const muiTextField = {
  defaultProps: {
    variant: "filled",
    // InputProps: { disableUnderline: true },  // see muiFilledInput
    InputLabelProps: {
      shrink: true,
    },
  },
  styleOverrides: {
    root: {
      ".MuiFilledInput-root": {
        borderRadius: 8,

        ".MuiFilledInput-input": {
          padding: "0 9px",
          height: 40,
          fontSize: "1rem",
          lineHeight: "1.5rem",
          letterSpacing: "-0.031rem", // -.5px
          borderRadius: 8,
          border: `1px solid ${theme.palette.divider}`,

          ":focus": {
            borderColor: theme.palette.primary.light,
            boxShadow: `0 0 0 2px ${theme.palette.primary.light}`,
            outline: "1px solid transparent", // Fallback. Will be visible with custom system colors in Forced Color Modes
          },

          "&.Mui-disabled": {
            color: "rgba(0, 0, 0, 0.38)", // TODO: add to palette
          },
        },

        "&.Mui-error": {
          backgroundColor: "rgba(253, 244, 242, 1)", // TODO: add color to palette
          ".MuiFilledInput-input": {
            borderColor: theme.palette.error.main,
            boxShadow: `0 0 0 1px #FF897A`,  // TODO: add color
            ":focus": {
              boxShadow: `0 0 0 2px ${theme.palette.error.main}`,
            },
          },
          ":hover": {
            backgroundColor: "rgba(0, 0, 0, .04)", // TODO: add color to palette
          }
        },
      },

      ".MuiInputLabel-filled": {
        color: theme.palette.text.secondary,
        top: "-0.375rem", // will shift up if Safari ups text size
        left: "-12px",// 
        "&.Mui-error": {
          color: theme.palette.text.secondary,
        },
        "&.Mui-disabled": {
          color: "rgba(0, 0, 0, 0.38)", // TODO: add to palette
        },
      },

      ".MuiFormHelperText-root": {
        marginLeft: 0,
        lineHeight: "1rem",

        "&.Mui-error": {
          ":before": {
            marginRight: "5px",
            content: `url("data:image/svg+xml,<svg width='10' viewBox='0 0 8 8' xmlns='http://www.w3.org/2000/svg'><circle cx='4' cy='4' r='4' fill='red'/></svg>")`,
          },
        },
        "&.Mui-disabled": {
          color: "rgba(0, 0, 0, 0.38)", // TODO: add to palette
        },
      },
    },
  },
};

export default muiTextField;
