import theme from '../core';

const muiButton = {
  defaultProps: {
    disableElevation: true,
    variant: "contained",
  },
  styleOverrides: {
    root: {
      boxShadow: "none",
      borderRadius: "8px",
      textTransform: "none",

      /* example: To customize hover...
      '&:active, &:hover': {
      	backgroundColor: theme.palette.primaryMainHover,
      },
      */

      /* example: To customize it when its disabled:
      '&.Mui-disabled': {
      	backgroundColor: '#e2e2e2',
      	color: '#c2c2c2',
      },
      */
    },
    contained: {
      // backgroundColor: 'secondary.main',
    },
    sizeSmall: {
      fontSize: "0.875rem",
      lineHeight: "1.5rem",
      fontWeight: theme.typography.fontWeightBold,
    },
    sizeMedium: {
      fontSize: "0.875rem",
      lineHeight: "1.5rem",
      fontWeight: theme.typography.fontWeightBold,
    },
    sizeLarge: {
      fontSize: "1.0rem",
      lineHeight: "1.5rem",
      fontWeight: theme.typography.fontWeightBold,
      letterSpacing: "-0.5px"
    },
  },
  /* example: Adding a custom variant prop, 
     (this customization would live in the app theme, not in Brio core)
  variants: [
    {
      props: { variant: "appHeader" },
      style: {
        width: "126px",
      },
    },
  ],
  */
};

export default muiButton;
