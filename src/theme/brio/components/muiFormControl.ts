// import theme from "../core";

const muiFormControl = {
  styleOverrides: {
    root: {
      "label + .MuiFilledInput-root" : {      // for customized TextField variant
        marginTop: '22px',
      },

    }
  },
};

export default muiFormControl;