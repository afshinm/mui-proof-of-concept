import theme from "../core";

const muiAppBar = {
  styleOverrides: {
    colorGraphite1: {
      backgroundColor : theme.palette.graphite1,

    }
  },
};

export default muiAppBar;