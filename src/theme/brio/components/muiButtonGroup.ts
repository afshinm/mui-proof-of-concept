import theme from '../core';

const muiButtonGroup = {
  defaultProps: {
    disableElevation: true,
    variant: "contained",
  },
  styleOverrides: {
    root: {
      boxShadow: "none",

    },
  },
};

export default muiButtonGroup;
