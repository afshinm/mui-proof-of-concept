// import theme from "../core";

const muiFilledInput = {
  defaultProps: {
    // InputProps: { disableUnderline: true },  // not working! hack below:
  },
  styleOverrides: {
    underline: {
      ":before": { display: "none" },
      ":after": { display: "none" },
    },
  },
};

export default muiFilledInput;
