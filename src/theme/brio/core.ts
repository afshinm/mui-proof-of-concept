import { createTheme } from "@mui/material/styles";
import React from "react";

import { brioColors } from "./colors";

// TODO: Put this in mui-theme.d.ts ??
declare module "@mui/material/styles" {
  interface Palette {
    graphite1: React.CSSProperties["color"];
    graphite2: React.CSSProperties["color"];
    graphite3: React.CSSProperties["color"];
    graphite4: React.CSSProperties["color"];
    graphite5: React.CSSProperties["color"];
    graphite6: React.CSSProperties["color"];
    graphite7: React.CSSProperties["color"];
    graphite8: React.CSSProperties["color"];
  }
  interface PaletteOptions {
    graphite1: React.CSSProperties["color"];
    graphite2: React.CSSProperties["color"];
    graphite3: React.CSSProperties["color"];
    graphite4: React.CSSProperties["color"];
    graphite5: React.CSSProperties["color"];
    graphite6: React.CSSProperties["color"];
    graphite7: React.CSSProperties["color"];
    graphite8: React.CSSProperties["color"];
  }

  // // https://mui.com/customization/palette/#adding-new-colors
  // interface PaletteColor {
  //   tertiary?: string;
  // }
  // interface SimplePaletteColorOptions {
  //   tertiary?: string;
  // }
}

const theme = createTheme({
  palette: {
    text: {
      primary: brioColors.TEXT_PRIMARY,
      secondary: brioColors.TEXT_SECONDARY,
      // tertiary: brioColors.TEXT_TERTIARY,
      disabled: brioColors.TEXT_DISABLED,
    },
    primary: {
      main: brioColors.PRIMARY_MAIN,
      light: brioColors.PRIMARY_LIGHT,
      dark: brioColors.PRIMARY_DARK,
    },
    secondary: {
      main: brioColors.SECONDARY_MAIN,
      light: brioColors.SECONDARY_LIGHT,
      dark: brioColors.SECONDARY_DARK,
    },
    error: {
      main: brioColors.ERROR_MAIN,
      light: brioColors.ERROR_LIGHT,
      dark: brioColors.ERROR_DARK,
    },
    warning: {
      main: brioColors.WARNING_MAIN,
      light: brioColors.WARNING_LIGHT,
      dark: brioColors.WARNING_DARK,      
    },
    info: {
      main: brioColors.INFO_MAIN,
      light: brioColors.INFO_LIGHT,
      dark: brioColors.INFO_DARK,      
    },
    success: {
      main: brioColors.SUCCESS_MAIN,
      light: brioColors.SUCCESS_LIGHT,
      dark: brioColors.SUCCESS_DARK,   
    },
    action: {
      active: brioColors.ACTION_ACTIVE,
      hover: brioColors.ACTION_HOVER,
      selected: brioColors.ACTION_SELECTED,
      disabled: brioColors.ACTION_DISABLED,
      disabledBackground: brioColors.ACTION_DISABLED_BG,
    },
    divider: brioColors.DIVIDER,

    // Create some new colors, see example use in components/muiButton.ts
    // TODO: might want to put this under a heading here, or move out of palettes...
    graphite1: brioColors.GRAPHITE1,
    graphite2: brioColors.GRAPHITE2,
    graphite3: brioColors.GRAPHITE3,
    graphite4: brioColors.GRAPHITE4,
    graphite5: brioColors.GRAPHITE5,
    graphite6: brioColors.GRAPHITE6,
    graphite7: brioColors.GRAPHITE7,
    graphite8: brioColors.GRAPHITE8,
  },
  shape: {     // For customizing most border-radius' in one place
    borderRadius: 1,
  },
  // spacing: 8,// Defining the px size of one unit
  // transitions: {
  // },

  /* Make sure to use <CssBaseline /> so that custom font is globally defined */
  typography: {
    fontFamily: "UntitledSans, Helvetica, sans-serif",
    fontWeightBold: 500, // TODO: terminology in designs b/w bold & mui 'regular'
  },
});

export default theme;
