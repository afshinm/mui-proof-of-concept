# MUI Proof of concept app

## A (CRA created) React app with MUI 5 for proof of concept, and to try out Brio's upcoming implementation.

Right now, this app implements of 3 pages which are implementations of:
- [this Figma mock](https://www.figma.com/file/mPLrcEBuf6NKkmuRwL0JB4/Ooma-Design-Workshop?node-id=760%3A12764) showing a proposed screen from Ooma Meetings,
- this [Integrations Server - connect](https://www.figma.com/file/ykdn2xIaurOQuEqXvULyTj/Desktop-App-%26-User-Portal?node-id=14807%3A225951) screen, and 
- this [Integrations Server - connection confirmation](https://www.figma.com/file/ykdn2xIaurOQuEqXvULyTj/Desktop-App-%26-User-Portal?node-id=14951%3A226509) screen

Mui gives us a rich set of components and a system that gives us a variety of options of how to customize the css for those components, and to some extent their functionality.  Brio adds Ooma's flavor on top of that.  Developers can then also add their own additions and modifications to Brio by following the patterns set in the Brio folder here.  While the definition of what is Brio is being finalized by design, this code shows how it can/will be implemented.

Basically all of Brio's customizations to Mui will live under `/theme/brio`, the exception is pulling in the font (global css file), and TS types definitions.  The code here also shows how the developer can wrap their own customizations around Brio, under the `/theme` directory.  

### *An Outline of whats where in the code:*

- `/index.css` - pulls Ooma Untitled Sans font in,
- `/index.tsx` - wraps app in theme provider and pulls in apps theme from theme directory,
- `/tsconfig.json` - has references to Brio types def file (HELP?),
- `/theme` - Brio is in here, as well as apps own mui customizations,
- `/theme/index.ts` - Customizations to Mui for whole app on top of Brio, or just passes Brio as theme.
- `/theme/theme.d.ts` - Some customizations will requires appending Mui's type definitions, which will go here.
- `/theme/brio` - Brio customizations to MUI
- `/theme/brio/index.ts` - Customizations to Mui; pulls in all the different components specific customizations
- `/theme/brio/core.ts` - Core customizations to Mui pulled into brio/index.ts above
-- fonts (UntitledSans), new colors and color over-rides (with type extensions (HELP?))
- `/theme/brio/version.xxx.txt` - Brio theme version. I'll increment this as I iterate. (HELP?)
- `/theme/brio/components/` - Directory contains a file for each customized Mui component
- - `/theme/brio/components/muiButton.ts` - example of customizing Button component
- - - changes default props to disableElevation and use contained variant, and change border Radius, etc..


### Mock Snapshots
![from Figma](./src/assets/figma.png)

![my version](./src/assets/connect.png)

![my version](./src/assets/txs.png)

---

# Running this React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).  You have to clone it to your local machine and build it, then run it.  

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
